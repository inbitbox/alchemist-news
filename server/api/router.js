var express = require('express');
var router = express.Router();
var controller = require('./');
var passport = require('passport')
var LocalStrategy = require('passport-local').Strategy;
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var JWT= require('express-jwt');

/****Danger!!!!!*****/
var goodToken= JWT({
  secret: new Buffer('Really Secret Secret', 'base64') 
});

passport.use('createUser', new LocalStrategy(
    {passReqToCallback: true},
    function(req, username, password, done){
      console.log('pp: createUser');
      controller.getUserbyName(username, function(err, user){
        // In case of any error return
            if (err){
              console.log('Error in SignUp: '+ err);
              return done(err);
            }
            // already exists
            if (user) {
              console.log('User already exists');
              return done(null, false) 
                 req.flash('message','User Already Exists');
            } else {
              // if there is no user with that email
              // create the user
                var newUser = {};
                // set the user's local credentials
                newUser.username = username;

                newUser.email = req.param('email');
                newUser.firstName = req.param('firstName');
                newUser.lastName = req.param('lastName');
                
                bcrypt.hash(req.body.password, 10, function(err, hash) {
                    newUser.password = hash;
                    console.log('hash: '+ hash)
                    controller.createUser(newUser, function(err, newUser) {
                        if (err){
                          console.log('Error in creating user: '+err);  
                          throw err;  
                        }else{
                            console.log('User Registration succesful');    
                            return done(null, newUser);
                        } 
                    });
                });  
            }
        });
    }
));
passport.use('login', new LocalStrategy(
    {passReqToCallback: true},
    function(req, username, password, done){
        console.log('pp: logginUser: ' + toString(username));
        controller.getUserbyName(username, function(err, user){
            if(err){return done(err);}
            if (!user){
                console.log('User Not Found with username: '+ username);
                return done(null, false);                 
            }else{
                bcrypt.compare(req.body.password, user.password, function(err, same){
                    if(err) console.log(err);
                    if(same==false){
                        console.log('Invalid Password');
                        return done(null, false);
                    }else{
                        console.log('This guy: ' + user.name + ' made it in');
                        return done(null, user);
                    }    
                });       
            }
        });        
    })
    
);

passport.serializeUser(function(user, done) {
  done(null, user.id);
});
 
passport.deserializeUser(function(id, done) {
  getUserbyId(id, function(err, user) {
    done(err, user);
  });
});

var isAuthenticated = function (req, res, next) {
  if (req.isAuthenticated())
    return next();
  res.redirect('/');
}

router.post('/createUser', function(req, res, next){
  console.log('api/createUser')
    passport.authenticate('createUser',function(err, newUser, info) {
      if (err) { return next(err) }
      if (!newUser){
          console.log('Whoops!')
          res.status(401).json({ error: 'User Creation Failed' });
      }else{
          var token = jwt.sign(newUser, new Buffer('Really Secret Secret', 'base64'), { expiresInMinutes: 60*5 }); //Danger
          res.status(200).json(token);
          console.log('It worked');
      }  
    })(req, res, next)   
});

router.post('/login',function(req, res, next){
    console.log(req.body.password);
    passport.authenticate('login', function(err, user, info){
    if (err) { return next(err) }
    if (!user){
        console.log('Whoops!')
        res.status(401).json({ error: 'User Not Found' });
    }else{
        var token = jwt.sign(user, new Buffer('Really Secret Secret', 'base64'), { expiresInMinutes: 60*5 });
        res.status(200).json(token);
        console.log('It worked');
    }
    })(req, res, next); 
});

router.post('/submit', goodToken, function(req, res){
  console.log('Submit')
  console.log(req.body.item);
  controller.createItem(req.body.item, function(err){
    if (err){res.status(401).json(err)}
    else{
      console.log('Say hooray!');
      res.sendStatus(200);
    }  
  });
});
router.get('/top', function(req, res){
  console.log(req.query.page)
  controller.getTopItems(req.query.page, function(err, stories){
    res.status(200).json(stories);
  });
});
router.post('/upvote', goodToken, function(req, res){
  console.log('req.user: ' + JSON.stringify(req.user));
  console.log('req.body.item_id: ' + req.body.item_id);
  controller.upVote(req.user.id, req.body.item_id, function(err, upvoted){
    
    if (err){res.status(401).json(err)}
      else{
        console.log('Say hooray!');
        res.status(200).json(upvoted.items_upvoted);
      } 
  })
});
router.get('/newest', function(req, res){
  controller.getNewest(req.params.page, function(err, stories){
    res.status(200).json(stories);
  });
});
router.get('/ask', function(req, res){
  controller.getQuestions(req.params.page, function(err, questions){
    res.status(200).json(questions);
  });
});
router.get('/show', function(req, res){
  controller.getDemos(req.params.page, function(err, demos){
    res.status(200).json(demos);
  });
});
router.post('/item', function(req, res){
  console.log('req.body.item_id ' + req.body.item_id);
  controller.getItembyId(req.body.item_id, function(err, item){
    res.status(200).json(item);
  });
});
router.post('/getFamily', function(req, res){
  console.log('req.body.item_id: ' + req.body.item_id);
  controller.buildFamily(req.body.item_id, function(err, family){
      if(err) {console.log('Error getting Family')}
      console.log('Sending Family: ' + JSON.stringify(family, null, 2));
      res.status(200).json(family);
  });
});

module.exports = router;