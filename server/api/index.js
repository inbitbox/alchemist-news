//api/index.js
"use strict";

var pg = require('pg');
var async = require('async');
var Client;
var connectionString = require('../config/local.env.js').connectionString;
pg.connect(connectionString, function(err, client, done){
    if(err) console.log(err);
    Client = client;
});
/*exports.callName = function(input, cb){
    console.log('API:callname');
    console.log('input:' + input);
    Client.query('SELECT * FROM some_table WHERE column = $1', [input], function(err, result){
        if(err){
            console.log(err);
            callback(err, null);
        }else{
            console.log('result: ' + JSON.stringify(result.rows));
            cb(null, result.rows);
        }
    });
};*/

exports.createUser = function(user, cb){
    console.log('API:createUser');
    console.log('user:' + JSON.stringify(user));
    Client.query('INSERT INTO users (email, username, password) VALUES ($1,$2,$3) RETURNING *',
        [user.email,user.username,user.password],
        function(err, result){
        if(err){
            console.log('API:createUser Error: ' + err);
            cb(err, null);
        }else{
            console.log('result: ' + JSON.stringify(result.rows[0]));
            cb(null, result.rows[0]);
        }
    });
};

exports.createItem = function(item, cb){
    console.log('API:createItem');
    console.log('input:' + JSON.stringify(item));
    Client.query('INSERT INTO items (title, by, item_type, company, body, parent, url) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING *',
        [item.title, item.by, item.type.toUpperCase(), item.company, item.body, item.parent, item.url],
        function(err, result){
        if(err){
            console.log('API:createItem ' + err);
            cb(err, null);
        }else{
            console.log('result: ' + JSON.stringify(result.rows[0]));
            cb(null, result.rows[0]);
        }
    });
};

exports.getItembyId = function(item_id, cb){
    console.log('API:getItembyId');
    console.log('item_id:' + item_id);
    Client.query('SELECT * FROM items WHERE item_id = $1', [item_id], function(err, result){
        if(err){
            console.log('API:getItembyId' + err);
            cb(err, null);
        }else{
            console.log('result: ' + JSON.stringify(result.rows[0]));
            cb(null, result.rows[0]);
        }
    });
};
exports.getUserbyName = function(username, cb){
    console.log('API:getUserbyName');
    console.log('username:' + username);
    Client.query('SELECT * FROM users WHERE username = $1', [username], function(err, result){
        if(err){
            console.log(err);
            cb(err, null);
        }else{
            console.log('result: ' + JSON.stringify(result.rows));
            cb(null, result.rows[0]);
        }
    });
};

exports.upVote = function(user_id, item_id, cb){
    console.log('API:Upvote');
    console.log(user_id + ' is upvoting item ' + item_id);
    Client.query('SELECT EXISTS(SELECT * FROM (SELECT items_upvoted FROM user_item_votes WHERE user_id = $1) AS U WHERE $2 = ANY(items_upvoted))', [user_id, item_id], function(err, result){
        if(err){
            console.log(err);
            cb(err, null);
        }else{
            if (result.rows[0].exists) {
                console.log(result.rows[0]);
                console.log("Already Upvoted");
            } else{
                Client.query('UPDATE user_item_votes SET items_upvoted = (array_append((SELECT items_upvoted from  user_item_votes WHERE user_id=$1), $2)) WHERE user_id=$1 RETURNING items_upvoted', [user_id, item_id], function(err, result){
                    if(err){
                        console.log(err);
                        cb(err, null);
                    }else{
                        console.log('items upvoted: ' + JSON.stringify(result.rows[0]));
                        cb(null, result.rows[0]);
                    }
                });
            };                 
        }
    });    
};

/*exports.getKids = function getkids(item, cback, kids, depth){
    console.log('getkids');
    if (!item.descendants){return cback(null, null)};
    var count=+item.descendants.length;
    
    var kids= kids || [];
    var depth= depth || 0;
    item.descendants.forEach(function(element, index, array){
        console.log('count: ' + count);
        console.log('element: ' + element)
        console.log('#kids: ' + array.length)

        exports.getItembyId(element, function(err, item){
            if(err){console.log(err)}
            else{
                console.log('item: ' + item);
                item.depth=depth;
                if(!item.descendants){
                    kids.push({parent: item});
                    count--
                    console.log('No descendants');
                    if(count==0){ return cback(null, kids);}
                }
                else{
                    console.log('descendants.length: ' + item.descendants.length)
                    count+=item.descendants.length;
                    console.log('kids: ' + kids);
                    exports.buildFamily(item.item_id, function(family){
                        kids.push(family);
                        count--;
                        if(count==0){ 
                            console.log('going back back back');
                            return mback(null, kids);}
                        console.log('deepcount: ' + count + 'depth: ' + depth + 'id: ' + item.item_id)
                    }, count);
                    
                }
            }
            
        });        
    });
};
exports.buildFamily = function (item_id, cb, count, mback){
    console.log('API:buildFamily');
    console.log('Parent:' + item_id);
    var count= count || 0;
    var family={};
    Client.query('SELECT * FROM items WHERE item_id = $1', [item_id], function(err, result){
        if(err){
            console.log(err);
            cb(err, null);
        }else{
            console.log('got Parent: ' + JSON.stringify(result.rows[0]));
            family.parent=result.rows[0]
            exports.getKids(family.parent, function(err, kids){
                if(err){
                    console.log('Error: ' + err)
                    cb(err, null)}
                else{
                    family.kids=kids;
                    console.log('Sending the family your way: ' + JSON.stringify(family));
                    console.log('cb: ' + cb);
                    cb(family);
                    if (count==0){mback(null, family);}
                    
                } 
            });            
        }
    });
};*/


/*var count=0;
exports.buildFamily = function(item_id, mback){
    var family={};
    console.log('buildingfamily')
    console.log('count at the begining' + count);
    exports.getItembyId(item_id, function(err, item){
        
        if(item.descendants){
            family=item;
            family.kids=[];

            count= parseInt(count)+parseInt(item.descendants.length);
            var addition= (parseInt(count) + parseInt(item.descendants.length));
            console.log('Addition: ' + ((+count) + (+item.descendants.length)));
            
            async.eachSeries(item.descendants, buildFamily, mback)
        }
        else{
            console.log('childless count' + count);
            if(count==0){ return mback(null, item);}
            else{console.log('I run'); console.log(cb); cb(item)}
        }
    });    
};*/

/*var count=0;
exports.buildFamily = function(item_id, mback){
    var extendedFamily={};

    exports.getItembyId(item_id, function(err, item){
        extendedFamily=item;
        if(item.descendants){
            console.log('#descendants: ' + item.descendants.length)
            extendedFamily.kids=[];
            count= count + item.descendants.length;
            console.log('outercount ' + count);
            async.eachSeries(item.descendants, function(item, callback){                
                count--
                console.log('item: ' + item);
                exports.buildFamily(item, function(err, family){
                    console.log('deepcount: ' + count);
                    extendedFamily.kids.push(family);
                    callback();
                    
                })
            }, mback(null, extendedFamily))        
        }
        else{
            count++
            if(count===0){ console.log('I run');  mback(null, extendedFamily);}
            else{
                mback(null, extendedFamily);
                
            }
        }
    });
};*/


/*var count=0;
exports.buildFamily = function(item_id, mback){
    var extendedFamily={};

    exports.getItembyId(item_id, function(err, item){
        extendedFamily=item;
        if(item.descendants){
            extendedFamily.kids=[];
            count= count + item.descendants.length;
            item.descendants.forEach(function(element, index, array){                
                count--
                exports.buildFamily(element, function(err, family){
                    console.log('deepcount: ' + count);
                    extendedFamily.kids.push(family)
                    if(count==0){ return mback(null, extendedFamily);}
                    else {extendedFamily.kids.push(family);}
                })
           });
        }
        else{
          if(count==0){ return mback(null, extendedFamily);}
          else{return;}
        }
    });

};*/

/*exports.buildFamily = function(item, done){
    console.log('API:buildFamily');
    console.log('done ' + done);
    var extendedFamily={}
    
    exports.getItembyId(item, function(err, item){
        extendedFamily=item;

        if(item.descendants){
            extendedFamily.kids=[];
            async.forEachOfSeries(item.descendants, function(value, index, callback){
                console.log('index: ' + index);                    
                    exports.getItembyId(value, function(err, item){
                        if(err){
                            callback(err);
                            console.log('Error getting kids' + err)
                        }else{
                            if(item.descendants){
                                exports.buildFamily(item.item_id, function(err, family){
                                    extendedFamily.kids.push(family);
                                });
                            }else{
                                extendedFamily.kids.push(item);
                                console.log('Family so far:' + JSON.stringify(extendedFamily));
                                callback(null, extendedFamily);
                            } 
                        }    
                    })
            }, done(null, extendedFamily));            
        }else{
            console.log('item ' + item.item_id + 'has no kids')
            done(null, extendedFamily);
        }
    })
};*/

exports.buildFamily = function(item_id, done){
    console.log('API:buildFamily');
    
    var extendedFamily={}
    exports.getItembyId(item_id, function(err, item){
        if(err){throw err}
        extendedFamily=item;
        if(item.descendants){
            extendedFamily.kids=[]
            async.eachSeries(item.descendants, function(item_id, callback){
                exports.getItembyId(item_id, function(err, item){
                    if(err){throw err}
                    if(item.descendants){
                        exports.buildFamily(item.item_id, function(err, family){
                            extendedFamily.kids.push(family);
                            callback();
                        })
                    }else{
                        extendedFamily.kids.push(item);
                        callback();
                    }                            
                })
            }, function(err){
                done(null, extendedFamily)
            })

        }else{
            done(null, extendedFamily)
        }
    });
}

exports.getNewest = function(page, cb){
    console.log('API:getNewest');
    console.log('page:' + page);
    Client.query('SELECT * FROM items WHERE item_type!=$$COMMENT$$ ORDER BY created DESC LIMIT 30 OFFSET 30*$1 ', [page], function(err, result){
        if(err){
            console.log(err);
            cb(err, null);
        }else{
            console.log('result: ' + JSON.stringify(result.rows));
            cb(null, result.rows);
        }
    });
};

exports.getQuestions = function(page, cb){
    console.log('API:getQuestions');
    console.log('page: ' + page);
    Client.query('SELECT * FROM items WHERE item_type = $$ASK$$ ORDER BY xsirion DESC LIMIT 30 OFFSET 30*$1 ', [page], function(err, result){
        if(err){
            console.log(err);
            cb(err, null);
        }else{
            console.log('result: ' + JSON.stringify(result.rows));
            cb(null, result.rows);
        }
    });
};
exports.getDemos = function(page, cb){
    console.log('API:getDemos');
    console.log('page: ' + page);
    Client.query('SELECT * FROM items WHERE item_type = $$SHOW$$ ORDER BY xsirion DESC LIMIT 30 OFFSET 30*$1 ', [page], function(err, result){
        if(err){
            console.log(err);
            cb(err, null);
        }else{
            console.log('result: ' + JSON.stringify(result.rows));
            cb(null, result.rows);
        }
    });
};
exports.getTopItems = function(page, cb){
    console.log('API:getTopItems');
    console.log('page:' + page);
    Client.query('SELECT * FROM items WHERE item_type!=$$COMMENT$$ ORDER BY xsirion DESC LIMIT 30 OFFSET 30*$1', [page], function(err, result){
        if(err){
            console.log(err);
            cb(err, null);
        }else{
            console.log('result: ' + JSON.stringify(result.rows));
            cb(null, result.rows);
        }
    });
};

