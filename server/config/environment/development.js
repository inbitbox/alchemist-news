'use strict';

// Development specific configuration
// ==================================
module.exports = {
  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/alchemistnews-dev'
  },

  seedDB: true,
  pg: {
    connectionString: 'pg://bitbox_user:testing123@localhost:5432/alchemistnews'
  }
};

