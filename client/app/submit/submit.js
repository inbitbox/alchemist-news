'use strict';

angular.module('alchemistNewsApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('submit', {
        url: '/submit',
        templateUrl: 'app/submit/submit.html',
        controller: 'SubmitCtrl'
      });
  })
  .controller('SubmitCtrl', function($scope, $window, $http, $state, upvoteService){
    $scope.window=$window;
    $scope.item={
        id: 1,
        type: 'Story',
        title:'',
        company: '',
        by:$window.localStorage.user,
        time: Date.now(),
        text:'',
        url: ''
    }
    $scope.item_types=['Story', 'Ask', 'Job', 'Show']
    $scope.submit = function(){
        var item = $scope.item;
        $http
          .post('api/submit', {item})
          .success(function(data, status, headers, config){
            console.log('successes')
            $state.go('newest');
          }) 
          .error(function(){
            $scope.errorMessage;
          })
    }
  });