'use strict';
//var JWTHelper = require('angular-jwt');
angular.module('alchemistNewsApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('login', {
            url: '/login',
            templateUrl: 'app/login/login.html',
            controller: 'LoginCtrl'
            })

  })
  .controller('LoginCtrl', function($scope, $state, $http, $window, jwtHelper){
    $scope.window=$window;
    $scope.login = function(){
        $http
            .post('api/login', $scope.user)
            .success(function(data, status, headers, config){
                $window.localStorage.setItem('sessionToken', data);
                $window.localStorage.setItem('user', jwtHelper.decodeToken(data).username);
                $window.localStorage.setItem('userId', jwtHelper.decodeToken(data).id);
                console.log('This guy: ' + JSON.stringify(jwtHelper.decodeToken(data)) + ' made it in');
                $state.go('main');
                $window.loggedIn=true;
                console.log($window.localStorage.user + ' is logged in? ' + $window.loggedIn );
                console.log(' window:  ' + $window.localStorage.user);
                console.log(' window:  ' + $window.localStorage.sessionToken);
            })
            .error(function (data, status, headers, config){
                delete $window.sessionStorage.token;
                $scope.message = 'Error: Invalid Username of Password';
                console.log('This guy: ' + JSON.stringify($scope.user) + ' did not make it in');
            });
    }

    $scope.createUser = function(){
        $http
          .post('api/createUser', $scope.newUser)
          .success(function(data, status, headers, config){
                $window.localStorage.setItem('sessionToken', data);
                $window.localStorage.setItem('username', jwtHelper.decodeToken(data).username);
                $window.localStorage.setItem('userId', jwtHelper.decodeToken(data).id);
                console.log('This guy: ' + JSON.stringify(jwtHelper.decodeToken(data)) + ' made it in');
                $state.go('main');
                $window.loggedIn=true;
                console.log($window.localStorage.username + ' is logged in? ' + $window.loggedIn );
                console.log(' window:  ' + $window.localStorage.username);
                console.log(' window:  ' + $window.localStorage.sessionToken);
            })
          .error(function (data, status, headers, config){
                delete $window.sessionStorage.token;
                $scope.message = 'Error: Invalid Username of Password';
                console.log('This guy: ' + JSON.stringify($scope.user) + ' did not make it in');
            });
    }

  });