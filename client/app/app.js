'use strict';

angular.module('alchemistNewsApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ui.router',
  'ui.bootstrap',
  'angular-jwt',
  'angularMoment',
  'angular-url-format'
])
  
  .config(function ($rootScopeProvider, $httpProvider, $windowProvider, jwtInterceptorProvider, $stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider
      .otherwise('/');
    $locationProvider.html5Mode(true);

    console.log('config: ' + localStorage.user);
    
    jwtInterceptorProvider.tokenGetter =  function($window) {
      console.log('Interceptor: ' + localStorage.sessionToken);
      return localStorage.getItem('sessionToken');
    };
    $httpProvider.interceptors.push('jwtInterceptor');
    $rootScopeProvider.logout = function(){
      $window.localStorage.clear();
      $stateProvider.state.go('main');
    }
  })
  .directive('comment', function($compile){
    return {
      restrict: "E",
      replace: true,
      templateUrl: 'comment-tree.html',
      scope: {
        comments: '=',
        comment: '=',
        depth: '='
      },
      link: function (scope, element, attrs) {
        //check if this member has children
        if (scope.comment.kids) {
          scope.depth = scope.depth + 1;
          // we need to tell angular to render the directive
          $compile("<comment  ng-repeat='comment in comment.kids' comment='comment' depth='depth'></comment>")(scope, function(cloned, scope){element.after(cloned);});
          console.log('rainbow')
        }
      }
    }
  })
  .factory('upvoteService', function($http, $window){

        return {
          submitUpvote: function(item_id){
            console.log('submitting upvote');
            console.log('userId: ' + $window.localStorage.userId + ' itemID: ' + item_id)
            console.log($window.localStorage)
            $http
              .post('api/upvote', {item_id})
              .success(function(data, status, headers, config){
                console.log('upvoted: ' + Array.from(data));
                $window.localStorage.setItem('upvoted', data);

              })
              .error(function(){console.log('Error upvoting.')});
          }
        }
  })
  /*.factory('userDataService', function($window){
    return{  
      user: $window.localStorage.getItem('user'),
      setUser: function(userData){
        console.log('loading user'); 
        $window.localStorage.setItem('user', userData);
      }
    }  
  })
  .factory('userSessionService',  function($window, jwtHelper){
    return{
      login: function (token){
        console.log('logging an alchemist in!');
        $window.localStorage.setItem('sessionToken', jwtHelper.decodeToken(token));
        
      },
      logout: function(){
        $window.localStorage.clear();
      },
      session: $window.localStorage.getItem('sessionToken'),

    }    
  });*/