'use strict';

angular.module('alchemistNewsApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('ask', {
        url: '/ask',
        templateUrl: 'app/ask/ask.html',
        controller: 'AskCtrl'
      });
  })
  .controller('AskCtrl', function($scope, $window, $http){
    $scope.window=$window;
    $scope.page=1;
    $http
      .get('api/ask/', $scope.page )
      .success(function(data, status, headers, config){
        console.log('Got the top questions');
        $scope.items = data
        $scope.page++

      })
      .error(function (data, status, headers, config){

      })
  });