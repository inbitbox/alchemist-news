'use strict';

angular.module('alchemistNewsApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('newest', {
        url: '/newest',
        templateUrl: 'app/newest/newest.html',
        controller: 'NewCtrl'
      });
  })
  .controller('NewCtrl', function($scope, $window, $http, upvoteService){
    $scope.window=$window;
    $scope.page
    $http
      .get('api/newest/', $scope.page )
      .success(function(data, status, headers, config){
        console.log('Got the newest stories');
        $scope.items = data
        $scope.page++
        console.log('Upvotes: ' + $scope.window.localStorage.upvotes);

      })
      .error(function (data, status, headers, config){

      });
    $scope.upvote=upvoteService.submitUpvote;
  });

  