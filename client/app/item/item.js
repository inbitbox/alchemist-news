'use strict';

angular.module('alchemistNewsApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('item', {
        url: '/item',
        templateUrl: 'app/item/item.html',
        controller: 'ItemCtrl'
      });
  })
  .controller('ItemCtrl', function($location, $window, $scope, $http, $state, upvoteService){
    $scope.window=$window;
    var item_id=$location.search().id;
    $http
        .post('api/getFamily', {item_id})
        .success(function(data, status, headers, config){
            console.log('successes; got family: ' + JSON.stringify(data, null, 2));
            $scope.item=data;
            $scope.new_comment={
                type: 'Comment',
                parent: data.item_id,
                by:$window.localStorage.user,
                body:''
            };
            $scope.comments=data.kids;
            console.log('comments: ' + data.kids);
            
        })
        .error(function(){
            $scope.item=false;
            console.log('Uh oh! Missing family')
        })
    $scope.addComment=function(){
        var item=$scope.new_comment
        $http.post('api/submit', {item})
          .success(function(data, status, headers, config){
            console.log('successes; added Comment')
            $window.location.reload();
          }) 
          .error(function(){
            $scope.errorMessage;
            console.log('new_comment: ' + JSON.stringify(new_comment));
          })
    }
    $scope.upvote=upvoteService.submitUpvote;

  });

  /*.controller('ItemCtrl', function ($location, $window, $scope, $http) {
    $scope.window=$window
    
    var item_id=$location.search().id;
    console.log('item_id: ' + item_id);
    //$scope.item;
    $scope.new_comment={
                id: 1,
                type: 'Comment',
                parent: '',
                title:'',
                company: '',
                by:$window.localStorage.user,
                time: Date.now(),
                text:'',
                url: ''
        }; 
    $http
        .post('api/item', {item_id})
        .success(function(data, status, headers, config){
            console.log('successes; got a story')
            $scope.item=data;
            $scope.new_comment.parent=$scope.item.id;
            
        })
        .error(function(){
            $scope.errorMessage;
        });
    
    $scope.addComment=function(){
        var item=$scope.new_comment
        $http.post('api/submit', {item})
          .success(function(data, status, headers, config){
            console.log('successes; added Comment')
            
          }) 
          .error(function(){
            $scope.errorMessage;
            console.log('new_comment' + JSON.stringify(new_comment));
          })
    }

  });*/