'use strict';

angular.module('alchemistNewsApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('jobs', {
        url: '/jobs',
        templateUrl: 'app/jobs/jobs.html',
        controller: 'JobsCtrl'
      });
  })
  .controller('JobsCtrl', function($scope, $window){
    $scope.window=$window;
  });