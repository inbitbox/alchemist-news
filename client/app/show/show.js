'use strict';

angular.module('alchemistNewsApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('show', {
        url: '/show',
        templateUrl: 'app/show/show.html',
        controller: 'ShowCtrl'
      });
  })
  .controller('ShowCtrl', function($http, $scope, $window){
    $scope.window=$window;
    $scope.page=1;
    $http
      .get('api/show/', $scope.page )
      .success(function(data, status, headers, config){
        console.log('Got the top demos');
        $scope.items = data
        $scope.page++

      })
      .error(function (data, status, headers, config){

      })
  });