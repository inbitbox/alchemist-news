'use strict';

angular.module('alchemistNewsApp')
  .controller('MainCtrl', function ($scope, $http, $window, upvoteService) {
    $scope.window=$window;
    $window.logout= function() {
      $window.localStorage.deleteItem('sessionToken');
      $window.localStorage.deleteItem('user');
      console.log('logging this bitch out!')
    }
    $scope.page=1;

    $http
      .get(`api/top?page={$scope.page}`,  )
      .success(function(data, status, headers, config){
        console.log('Got the top stories');
        console.log($scope.page)
        $scope.items = data
        $scope.page++

      })
      .error(function (data, status, headers, config){

      });
      $scope.upvote=upvoteService.submitUpvote;

  });
