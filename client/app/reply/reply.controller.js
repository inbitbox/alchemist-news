'use strict';

angular.module('alchemistNewsApp')
  .controller('ReplyCtrl', function($location, $window, $scope, $http, $state){
    $scope.window=$window;
    var item_id=$location.search().id;
    $http
        .post('api/getFamily', {item_id})
        .success(function(data, status, headers, config){
            console.log('successes; got a story');
            $scope.item=data;
            $scope.new_comment={
                type: 'Comment',
                parent: data.item_id,
                by:$window.localStorage.user,
                body:''
            };
            $scope.comments=data.kids;
        })
    
    $scope.addComment=function(){
        var item=$scope.new_comment
        $http.post('api/submit', {item})
          .success(function(data, status, headers, config){
            console.log('successes; added Comment')
            $window.location.reload();
          }) 
          .error(function(){
            $scope.errorMessage;
            console.log('new_comment: ' + JSON.stringify(new_comment));
          })
    }
  });
