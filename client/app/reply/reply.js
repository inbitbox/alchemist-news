'use strict';

angular.module('alchemistNewsApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('reply', {
        url: '/reply',
        templateUrl: 'app/reply/reply.html',
        controller: 'ReplyCtrl'
      });
  });