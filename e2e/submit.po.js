'use strict';

var submitPage = function(){
    this.title = element(by.name('title'));
    this.url = element(by.name('url'));
    this.storyType = element(by.name('type'));
};

module.exports = new MainPage();
