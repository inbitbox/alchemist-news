'use strict';

describe('Main View', function() {
  var page;

  beforeEach(function() {
    browser.get('/');
    page = require('./main.po');
  });

  it('should include jumbotron with correct data', function() {
    expect(page.h1El.getText()).toBe('\'Allo, \'Allo!');
    expect(page.imgEl.getAttribute('src')).toMatch(/assets\/images\/yeoman.png$/);
    expect(page.imgEl.getAttribute('alt')).toBe('I\'m Yeoman');
  });
});

describe('Submit Page', function(){
  var page;

  beforeEach(function(){
    browser.get('/submit');
    submitPage = require('./submit.po');
  });
  it('should appear on the newest page after submit', function(){
    submitPage.title.sendKeys('Adam Back on 3 Forms of Centralization That Have Crept Into Bitcoin');
    submitPage.url.sendKeys('https://bitcoinmagazine.com/articles/adam-back-3-forms-centralization-crept-bitcoin-1442018963');
    expect(element(by.css('#items')).element(by.binding(item.item_id)).isPresent().toBe(true));
  });
  

  sendkeys()
  . . .
  Story ASK JOB SHOW

})